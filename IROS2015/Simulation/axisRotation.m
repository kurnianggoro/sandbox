function [ R ] = axisRotation( axis,angle )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

tetha=angle/180*pi;

u=axis;
u=u/norm(u);
ux=u(1);uy=u(2);uz=u(3);

R=[cos(tetha)+ux^2*(1-cos(tetha)) ux*uy*(1-cos(tetha))-uz*sin(tetha) ux*uz*(1-cos(tetha))+uy*sin(tetha)
   ux*uy*(1-cos(tetha))+uz*sin(tetha) cos(tetha)+uy^2*(1-cos(tetha)) uy*uz*(1-cos(tetha))-ux*sin(tetha)
   ux*uz*(1-cos(tetha))-uy*sin(tetha) uy*uz*(1-cos(tetha))+ux*sin(tetha) cos(tetha)+uz^2*(1-cos(tetha))];


end

