function [ deg_err, pos_err, pos_err_opt ] = sim_calib(sigma, n_orientation, distance,radius,pattern_size,optimize,draw,optim_iter)
%all measurements are in cm
%pattern size is half of the side length 

  % clear    
     if(nargin<8)
         optim_iter=99;
     end
     if(nargin<7)
         draw=0;
     end
     if(nargin<6)
         optimize=1;
     end
     if(nargin==0)
        sigma=0.12;n_orientation=20;distance=200;radius=10;pattern_size=50;optimize=1;draw=1;optim_iter=99;
     end
    
    warning ('off','all');
    
    varlist = {'P0data','Pi_data','nv','u_sol','rot_angles','orientations','sv','npts','PiValid'};
    clear(varlist{:})
    
    global P0data;
    global Pi_data;
    global nv;  
    global u_sol;
    global rot_angles;
    global orientations;
    global sv;
    global npts;
    global PiValid;
    
    orientations=n_orientation;
    
    u=[ sign(randn)*rand 1 sign(randn)*rand]';

    u=u/norm(u);
    allscanrange=[-150:150];
    angles=[-180:180];
    rot_angles=angles;
    rot_range=150;
    
    %% compute the transformation matrix H
    u=u/norm(u);
    y_axis=[0 1 0]';
    y_axis=y_axis/norm(y_axis);
    v=cross(y_axis,u);
    s=norm(v);
    c=dot(u,y_axis);

    v=v/s;
    w=cross(v,u);
    w=w/norm(w);
    
    x_axis=[1 0 0]';
    z_axis=[0 0 1]';

    c1=(x_axis+y_axis+z_axis)/3;
    c2=(u+v+w)/3;

    M=(x_axis-c1)*(v-c2)';
    M=M+(y_axis-c1)*(u-c2)';
    M=M+(z_axis-c1)*(w-c2)';
    [svd_u,svd_s,svd_v]=svd(M);
    H=svd_v*svd_u';
    if(det(H)<0)
       svd_v(:,3)=-svd_v(:,3);
       H=svd_v*svd_u';
    end
    [H*y_axis u H*[1 0 0]' v H*[0 0 1]' w];
    
    %% generate the translation vector
    %t=-v*radius;
    t=rand*v+rand*w;
    t=t/norm(t)*radius;

    %% generate the scanning vectors
    for j=1:length(allscanrange)
        r=axisRotation([1 0 0],allscanrange(j));
        sv(j,:)=r*[0 0 1]';
        sv(j,:)=sv(j,:)/norm(sv(j,:));
    end

    %% generate Si
    for i=1:length(angles)
        R=axisRotation(u,angles(i));
        Si(i,:)=R*t-t;
        facing(i,:)=R*sv(find(allscanrange==0,1),:)';
        if(angles(i)==0)
           front=facing(i,:)'; 
        end
    end
    
    
    %% generate the surface normals
    for i=1:n_orientation
        n=[sign(randn)*rand*0.5 sign(randn)*rand*0.5 1]';
        n_oy=cross(n,[1 0 0]');
        n_ox=cross(n,n_oy);
        
        %normalize
        n_ox=n_ox/norm(n_ox);
        n_oy=n_oy/norm(n_oy);
        n=n/norm(n);
        nv(i,:)=n; 
        
        rot_0=find(rot_angles==0,1)+round(sign(randn)*rand*2);
        pitch_0=find(allscanrange==0,1)+round(sign(randn)*rand*2);
        R=axisRotation(u,rot_angles(rot_0));
        ray=R*sv(pitch_0,:)'; 
        P0=Si(rot_0,:)'+R*(sv(pitch_0,:)'*(distance+randn*20));
        %P0=[randn randn distance+randn]';
        P0data(i,:)=P0;
        OX(i,:)=n_ox;
        OY(i,:)=n_oy;
        
    end
    
    
    %% main function 
    n_eq=0;
    disp('Generating simulation data');
    for orientation=1:n_orientation
        disp(strcat('Orientation #',num2str(orientation),'/',num2str(n_orientation)));
        n=nv(orientation,:)';
        P0=P0data(orientation,:)';
        N=n;
               
        %% generate laser range data 
        for i=find(rot_angles==-rot_range,1):find(rot_angles==rot_range,1)
            R=axisRotation(u,rot_angles(i));
            for j=1:length(allscanrange)
               ray=R*sv(j,:)'; 
               d=norm(n'*(P0-Si(i,:)'));
               L(orientation,i,j)=d/(n'*ray);
            end
        end

        %% generate GT points 
        for i=find(rot_angles==-rot_range,1):find(rot_angles==rot_range,1)
           R=axisRotation(u,rot_angles(i));
           for j=1:length(allscanrange)
              GT(orientation,:,i,j)=Si(i,:)'+R*(sv(j,:)*L(orientation,i,j))';
           end
        end


        %% Linear Programming Equation
        k=0;
        pivot_x(orientation)=round(rand*0.9*pattern_size);
        pivot_y(orientation)=round(rand*0.9*pattern_size);
        for i=find(rot_angles==-rot_range,1):find(rot_angles==rot_range,1)
           for j=1:length(allscanrange)
              
               p=GT(orientation,:,i,j)';

               dx=OX(orientation,:)*(p-P0);
               dy=OY(orientation,:)*(p-P0);
               
               if((dx<=0 && dx>=-pivot_x(orientation) && ((dy<=0 && dy>=-pivot_y(orientation))||(dy>=0 && dy<=pattern_size*2-pivot_y(orientation)))) ...
                  ||(dx>=0 && dx<=pattern_size*2-pivot_x(orientation) && ((dy<=0 && dy>=-pivot_y(orientation))||(dy>=0 && dy<=pattern_size*2-pivot_y(orientation))))...
                  && L(orientation,i,j)>0 ...
                  && dot(facing(i,:)',front)>0 ...
               )
                  k=k+1;
                  
                  GT_valid(:,orientation,k)=p;
              
                  n_eq=n_eq+1;
                  
                  pos=i;bin=j;
                  theta=rot_angles(pos)/180*pi;
                  R=axisRotation(u,rot_angles(pos));
                  r_2d=axisRotation([0 1 0]',rot_angles(pos));
                  Pi=(sv(bin,:)*(L(orientation,pos,bin)+randn*sigma))';
                  Pi_data(orientation,:,i,j)=Pi;
                  sint=sin(theta);
                  cost=1-cos(theta);

                  PiValid.pts(:,orientation,k)=Pi;
                  PiValid.phi(orientation,k)=rot_angles(pos);
                  PiValid.theta(orientation,k)=theta;
                  
                  Px=Pi(1);
                  Py=Pi(2);
                  Pz=Pi(3);
                  nx=N(1);
                  ny=N(2);
                  nz=N(3);

                  u=u/norm(u);
                  ux=u(1);uy=u(2);uz=u(3);


                  Q=[Py*nx*cost;-sint*Py*nx;
                    Py*ny*cost;
                    Py*nz*cost;Py*nz*sint;
                    Pz*nx*cost;Pz*nx*sint;
                    Pz*ny*cost;-Pz*ny*sint
                    Pz*nz*cost;
                    nx*cos(theta);
                    %nx*cost;
                    nx*sint;
                    nx;
                    ny*cos(theta);
                    ny*sint;
                    ny;
                    nz*cos(theta);
                    nz*sint;
                    nz
                    ];

                 tail=[t(1)-(t(1)*ux^2+t(2)*ux*uy+t(3)*ux*uz);-t(2)*uz+t(3)*uy;t(1)*ux^2+t(2)*ux*uy+t(3)*ux*uz-t(1);
                     t(2)-(ux*uy*t(1)+uy^2*t(2)+uy*uz*t(3));uz*t(1)-ux*t(3);ux*uy*t(1)+uy^2*t(2)+uy*uz*t(3)-t(2);
                     t(3)-(uz*ux*t(1)+uz*uy*t(2)+uz^2*t(3));-uy*t(1)+ux*t(2);uz*ux*t(1)+uz*uy*t(2)+uz^2*t(3)-t(3)];

                Rw=[ux*uy;uz;
                     uy^2;
                     uy*uz;ux;
                     ux*uz;uy;
                     uy*uz;ux;
                     uz^2];

                SS=[Rw; tail;];

                W(n_eq,:)=[Q'];
                S(n_eq,1)=N'*P0-Pz*nz*cos(theta)-Py*ny*cos(theta);
                CHECK(n_eq,:)=[W(n_eq,:)*SS S(n_eq,1)]; % the value are identical for each pair if measuremet error is ignored (sigma=0)
              end

           end
        end
        
        npts(orientation)=k;
    end
    
    disp('solving the linear equation');
    %% linear solution
    sol=W\S;
    [W\S SS ];
    
    u_sol=[sol(5) sol(7) sol(2)]';
    u_sol=u_sol/norm(u_sol);
    usol=[u_sol u];

    t_sol=[-sol(18)/sol(7) 0 sol(12)/sol(7)]';
    
    initial_params=[u' t';u_sol' t_sol'];
    
    %% optimization
    disp(strcat('Optimizing... (max iteration:',num2str(optim_iter),')'));
    deg_err=acos(dot(u,u_sol))/pi*180;
    pos_err=norm(t-t_sol);
    pos_err_opt=pos_err;
    residual=inf;
    if(optimize==1)
        options = optimset('Algorithm','levenberg-marquardt');
        options = optimset(options,'MaxFunEvals', optim_iter); 
        
        Vars=[t_sol];
        [V residual]=lsqnonlin(@computeErr,Vars,[],[],options);
        pos_err_opt=norm(t-V);
        optimized_params=[u' t';u_sol' t_sol';u_sol' V']
                
        u_draw=u_sol;
        t_draw=V;
    else
        u_draw=u_sol;
        t_draw=t_sol; 
    end

    %% drawing
    disp('Computing the error...')
    err=0;
    allpts=0;
    for orientation=1:n_orientation
       for k=1:npts(orientation)
           p=GT_valid(:,orientation,k);
           
           R=axisRotation(u_draw,PiValid.phi(orientation,k));
           p2=R*(PiValid.pts(:,orientation,k)+t_draw)-t_draw;

           err=err+norm(p-p2);          
           allpts=allpts+1;   
       end
    end
    
    errors=[err/allpts residual pos_err_opt deg_err];
    optim_res.average_point_err=errors(1);
    optim_res.residual=errors(2);
    optim_res.translation_err=errors(3);
    optim_res.orientation_err=errors(4);
    
    optim_res
   
    %% drawing
    if(draw)
    disp('Drawing...')
    
    img = imread('checkerboard.jpg');     %# Load a sample image
    Rotate_coordinate=axisRotation([1 0 0],90);
    Rotate_coordinate(2,:)=-Rotate_coordinate(2,:);  
    
    close all;
    for o=1:n_orientation
       %figure('Name',strcat('Orientation ',num2str(o)),'NumberTitle','off')
       hold on;
       
       %% draw rotation path (Si)
       u_vector_draw=Rotate_coordinate*u*20;
       t_vector_draw=Rotate_coordinate*t;
       quiver3(-t_vector_draw(1),-t_vector_draw(2),-t_vector_draw(3),u_vector_draw(1),u_vector_draw(2),u_vector_draw(3));
       for i=1:length(angles)
           Si_draw=Rotate_coordinate*Si(i,:)';%interchange y and z coordinate for better visualization
           scatter3(Si_draw(1),Si_draw(2),Si_draw(3),3,'b','filled');
       end
    
       patternToDraw=o;
       P0=P0data(o,:)';
              
       for k=1:npts(o)
           p=GT_valid(:,o,k);
           
           R=axisRotation(u_draw,PiValid.phi(o,k));
           p2=R*(PiValid.pts(:,o,k)+t_draw)-t_draw;

           
           if(o==patternToDraw)
              p=Rotate_coordinate*p;
              p2=Rotate_coordinate*p2; %interchange y and z coordinate for better visualization
              %scatter3(p(1),p(2),p(3),4,'r','filled'); %ground-truth
              scatter3(p2(1),p2(2),p2(3),3,'g','filled'); %calibration result    
                            
              %% uncalibrated
              %R=axisRotation([0 1 0]',PiValid.phi(o,k));
              %p3=Rotate_coordinate*R*PiValid.pts(:,o,k);
              %scatter3(p3(1),p3(2),p3(3),4,'b','filled');
           end
       end
       
        %% draw checkerboard
        dx=pivot_x(o);
        dy=pivot_y(o);
        
        P0=P0data(o,:)';
         
        P1=P0-OX(o,:)'*(dx)-OY(o,:)'*(dy);
        P2=P0-OX(o,:)'*(dx)+OY(o,:)'*(pattern_size*2-dy);
        P3=P0+OX(o,:)'*(pattern_size*2-dx)-OY(o,:)'*(dy);
        P4=P0+OX(o,:)'*(pattern_size*2-dx)+OY(o,:)'*(pattern_size*2-dy);
        
        %% interchange y and z coordinate for better visualization
        P1=Rotate_coordinate*P1;
        P2=Rotate_coordinate*P2;
        P3=Rotate_coordinate*P3;
        P4=Rotate_coordinate*P4;
        
        xImage = [P1(1) P3(1); P2(1) P4(1)];   
        yImage = [P1(2) P3(2); P2(2) P4(2)];           
        zImage = [P1(3) P3(3); P2(3) P4(3)];   
        
        if(o==patternToDraw)
            surf(xImage,yImage,zImage,...    %# Plot the surface
                 'CData',img,...
                 'FaceColor','texturemap');
        end       

        grid on
        axis equal

        xlabel('x');
        ylabel('z');
        zlabel('y');
    end
    end
    
    simulated_P0data=P0data;
    simulated_PiValid=PiValid;
    simulated_nv=nv;  
    simulated_npts=npts;
    simulated_u=u;
    simulated_t=t;
    
    
    %save('simulated_pts.mat','simulated_PiValid','simulated_npts','simulated_P0data','simulated_nv','simulated_u','simulated_t');

end