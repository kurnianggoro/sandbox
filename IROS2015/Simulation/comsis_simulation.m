clear;
close all;

sigma=0.12;
n_planes=30;

orientations=[2:n_planes];

% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 14)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 14)


% for ori=orientations
%     ori
%     for i=1:100
%        [orient,pos]=comsis_calc(sigma,ori,200);
%        ori_err(i)=orient;
%        pos_err(i)=pos;
%     end
%     ERR_ori(ori,:)=ori_err;
%     ERR_pos(ori,:)=pos_err;
%     ori_mean_err(ori)=mean(ori_err);
%     pos_mean_err(ori)=mean(pos_err);
% end
% 
% figure();plot([10:30],ori_mean_err(10:30),'LineWidth',2);grid on;
% xlabel('# poses of the calibration planes');ylabel('orientation error (degree)');
% figure();plot([10:30],pos_mean_err(10:30),'LineWidth',2);grid on;hold on;
% xlabel('# poses of the calibration planes');ylabel('translation error (cm)');
% axis([10 30 0 1])

sigmas=[sigma:0.01:0.3];
k=0;
for par=sigmas
    k=k+1;
    iter=[k par]
    for i=1:100
       [orient,pos,pos_opt]=comsis_calc_opt(par,30,200,10,50,1);
       ori_err(i)=orient;
       pos_err(i)=pos_opt;
    end
    ERR_ori_s(k,:)=ori_err;
    ERR_pos_s(k,:)=pos_err;

    ori_mean_err_s(k)=mean(ori_err);
    pos_mean_err_s(k)=mean(pos_err);
    
    ori_med_err_s(k)=median(ori_err);
    pos_med_err_s(k)=median(pos_err);
end

figure();plot(sigmas,ori_mean_err_s,'LineWidth',2);grid on;
axis([0.12 0.3 0 0.5])
xlabel('standard deviation of the measurement error (cm)');ylabel('orientation error (degree)');
figure();plot(sigmas,pos_mean_err_s,'LineWidth',2);grid on;
xlabel('standard deviation of the measurement error (cm)');ylabel('translation error (cm)');
axis([0.12 0.3 0 3])

% radius=[4:20];
% for rad=radius
%     rad
%     for i=1:100
%        [orient,pos]=comsis_calc(0.12,9,200,rad);
%        ori_err(i)=orient;
%        pos_err(i)=pos;
%     end
%     ERR_ori(rad,:)=ori_err;
%     ERR_pos(rad,:)=pos_err;
%     ori_mean_err(rad)=mean(ori_err);
%     pos_mean_err(rad)=mean(pos_err);
% end
% 
% figure();semilogy(radius,ori_mean_err(radius));grid on;
% xlabel('calibration pattern distance (cm)');ylabel('orientation error (degree)');
% figure();semilogy(radius,pos_mean_err(radius));grid on;hold on;
% xlabel('calibration pattern distance (cm)');ylabel('translation error (cm)');

% pattern_size=[20:10:100];
% for sz=pattern_size
%     sz
%     for i=1:100
%        [orient,pos]=comsis_calc_opt(0.12,9,200,4,sz,0);
%        ori_err(i)=orient;
%        pos_err(i)=pos;
%     end
%     ERR_ori(sz,:)=ori_err;
%     ERR_pos(sz,:)=pos_err;
%     ori_mean_err(sz)=mean(ori_err);
%     pos_mean_err(sz)=mean(pos_err);
% end
% 
% figure();plot(pattern_size*2,ori_mean_err(pattern_size),'LineWidth',2);grid on;
% xlabel('calibration pattern side length (cm)');ylabel('orientation error (degree)');
% figure();plot(pattern_size*2,pos_mean_err(pattern_size),'LineWidth',2);grid on;hold on;
% xlabel('calibration pattern side length (cm)');ylabel('translation error (cm)');

% distances=[50:10:200];
% for dist=distances
%     dist
%     for i=1:100
%        [orient,pos]=comsis_calc_opt(0.12,9,dist,4,50,0);
%        ori_err(i)=orient;
%        pos_err(i)=pos;
%     end
%     ERR_ori(dist,:)=ori_err;
%     ERR_pos(dist,:)=pos_err;
%     ori_mean_err(dist)=mean(ori_err);
%     pos_mean_err(dist)=mean(pos_err);
% end
% 
% figure();plot(distances,ori_mean_err(distances),'LineWidth',2);grid on;
% xlabel('distance of the calibration pattern to the origin (cm)');ylabel('orientation error (degree)');
% figure();plot(distances,pos_mean_err(distances),'LineWidth',2 );grid on;hold on;
% xlabel('distance of the calibration pattern to the origin (cm)');ylabel('translation error (cm)');
