function [ deg_err, pos_err, pos_err_opt,M_data ,g_data, t,g_data2 ] = comsis_calc(sigma, n_orientation, distance,radius,pattern_size,optimize)
%all measurements are in cm
%   comsis_calc(sigma, n_orientation, distance,radius,pattern_size(half_side_length),optimize)
    warning ('off','all');
    %radius=4;
    
%     varlist = {'P0data','Pi_data','nv','u_sol','rot_angles','orientations','sv'};
%     clear(varlist{:})

    global P0data;
    global Pi_data;
    global nv;  
    global u_sol;
    global rot_angles;
    global orientations;
    global sv;
    global max_phi;
    global max_theta;
    
    orientations=n_orientation;
    
    u=[ sign(randn)*rand 1 sign(randn)*rand]';

    u=u/norm(u);
    allscanrange=[0:180];
    scanrange=[0:35];
    angles=[0:360];
    rot_angles=angles;
    
    %% compute the transformation matrix
    u=u/norm(u);
    y_axis=[0 1 0]';
    y_axis=y_axis/norm(y_axis);
    v=cross(y_axis,u);
    s=norm(v);
    c=dot(u,y_axis);

    v=v/s;
    w=cross(v,u);
    w=w/norm(w);
    
    x_axis=[1 0 0]';
    z_axis=[0 0 1]';

    c1=(x_axis+y_axis+z_axis)/3;
    c2=(u+v+w)/3;

    M=(x_axis-c1)*(v-c2)';
    M=M+(y_axis-c1)*(u-c2)';
    M=M+(z_axis-c1)*(w-c2)';
    [svd_u,svd_s,svd_v]=svd(M);
    H=svd_v*svd_u';
    if(det(H)<0)
       svd_v(:,3)=-svd_v(:,3);
       H=svd_v*svd_u';
    end
    [H*y_axis u H*[1 0 0]' v H*[0 0 1]' w];
    

    %t=-v*radius;
    t=rand*v+rand*w;
    t=t/norm(t)*radius;

    %% generate the surface normals
    for i=1:n_orientation
        n=[randn*0.5 randn*0.5 1]';
        n_oy=cross(n,[1 0 0]');
        n_ox=cross(n,n_oy);
        
        %normalize
        n_ox=n_ox/norm(n_ox);
        n_oy=n_oy/norm(n_oy);
        n=n/norm(n);
        nv(i,:)=n; 
        
        P0=[randn randn distance+randn]';
        P0data(i,:)=P0;
        OX(i,:)=n_ox;
        OY(i,:)=n_oy;
        
    end
    
    %% find the number of scanning angles
    for i=1:n_orientation
        n=nv(i,:)';
        P0=P0data(i,:)';
        
        found=0;max_phi=0;max_theta=0;p_calib_pat=[0 0 0]';
        found1=0;found2=0;
        while (found2==0)
            
            R=axisRotation(u,angles(max_phi+1));
            r=axisRotation([1 0 0],allscanrange(max_theta+1));
            sv_t=r*[0 0 1]';
            sv_t=sv_t/norm(sv_t);


            ray=R*sv_t; 
            d=norm(n'*(P0-(R*t-t)));
            L_t=d/(n'*ray);

            p_calib_pat=R*t-t+R*(sv_t*L_t);
                        
   
            if(abs(dot(p_calib_pat-P0,OY(i,:)'))<=pattern_size  && found2==0)
                max_theta=max_theta+1;
                
            end
            if(max_theta>90 || abs(dot(p_calib_pat-P0,OY(i,:)'))>=pattern_size)
               found2=1; 
            end
            
        end
        
        %% find max phi
        while (found1==0)
            
            R=axisRotation(u,angles(max_phi+1));
            r=axisRotation([1 0 0],allscanrange(max_theta+1));
            sv_t=r*[0 0 1]';
            sv_t=sv_t/norm(sv_t);


            ray=R*sv_t; 
            d=norm(n'*(P0-(R*t-t)));
            L_t=d/(n'*ray);

            p_calib_pat=R*t-t+R*(sv_t*L_t);
                        
   
            if(abs(dot(p_calib_pat-P0,OX(i,:)'))<=pattern_size  && found1==0)
                max_phi=max_phi+1;
                
            end
            if(max_phi>40 || abs(dot(p_calib_pat-P0,OX(i,:)'))>=pattern_size)
               found1=1; 
            end
            
        end
        [max_phi max_theta dot(p_calib_pat-P0,OX(i,:)') dot(p_calib_pat-P0, n_oy)];
        
        maxphi(i)=max_phi;
        maxtheta(i)=max_theta;
    end
    
    [max_phi max_theta];

    %% generate the scanning vectors
    for j=1:length(allscanrange)
        r=axisRotation([1 0 0],allscanrange(j));
        sv(j,:)=r*[0 0 1]';
        sv(j,:)=sv(j,:)/norm(sv(j,:));
    end

    %% generate Si
    for i=1:length(angles)
        R=axisRotation(u,angles(i));
        Si(i,:)=R*t-t;
    end

    n_eq=0;

    for orientation=1:n_orientation
        
        n=nv(orientation,:)';
        
        P0=P0data(orientation,:)';
        N=n;
        
        
        

        %% generate laser range data 
        for i=1:maxphi(orientation) %length(rot_angles)
            R=axisRotation(u,rot_angles(i));
            for j=1:maxtheta(orientation)%length(scanrange)
               ray=R*sv(j,:)'; 
               d=norm(n'*(P0-Si(i,:)'));
               L(orientation,i,j)=d/(n'*ray);
            end
        end

        %% generate GT points 
        for i=1:maxphi(orientation)%length(rot_angles)
           R=axisRotation(u,rot_angles(i));
           for j=1:maxtheta(orientation)%length(scanrange)
              GT(orientation,:,i,j)=Si(i,:)'+R*(sv(j,:)*L(orientation,i,j))';
           end
        end
%    err=(H*n)'*(H*GT(orientation,:,i,j)'+H*t-(H*P0+H*t))  ;

        %% Linear Programming Equation
        for i=1:maxphi(orientation)%max(max_phi-20,2):max_phi
           for j=1:maxtheta(orientation)%2:max_theta
              pos=i;bin=j;
              tetha=rot_angles(pos)/180*pi;
              R=axisRotation(u,rot_angles(pos));
              r_2d=axisRotation([0 1 0]',rot_angles(pos));
              Pi=(sv(bin,:)*(L(orientation,pos,bin)+randn*sigma))';
              Pi_data(orientation,:,i,j)=Pi;
              sint=sin(tetha);
              cost=1-cos(tetha);

              Px=Pi(1);
              Py=Pi(2);
              Pz=Pi(3);
              nx=N(1);
              ny=N(2);
              nz=N(3);

              u=u/norm(u);
              ux=u(1);uy=u(2);uz=u(3);


            Q=[Py*nx*cost;-sint*Py*nx;
            Py*ny*cost;
            Py*nz*cost;Py*nz*sint;
            Pz*nx*cost;Pz*nx*sint;
            Pz*ny*cost;-Pz*ny*sint
            Pz*nz*cost;
            nx*cos(tetha);
            %nx*cost;
            nx*sint;
            nx;
            ny*cos(tetha);
            ny*sint;
            ny;
            nz*cos(tetha);
            nz*sint;
            nz
            ];

         tail=[t(1)-(t(1)*ux^2+t(2)*ux*uy+t(3)*ux*uz);-t(2)*uz+t(3)*uy;t(1)*ux^2+t(2)*ux*uy+t(3)*ux*uz-t(1);
             t(2)-(ux*uy*t(1)+uy^2*t(2)+uy*uz*t(3));uz*t(1)-ux*t(3);ux*uy*t(1)+uy^2*t(2)+uy*uz*t(3)-t(2);
             t(3)-(uz*ux*t(1)+uz*uy*t(2)+uz^2*t(3));-uy*t(1)+ux*t(2);uz*ux*t(1)+uz*uy*t(2)+uz^2*t(3)-t(3)];

        Rw=[ux*uy;uz;
             uy^2;
             uy*uz;ux;
             ux*uz;uy;
             uy*uz;ux;
             uz^2];

        SS=[Rw; tail;];

        n_eq=n_eq+1;   
        W(n_eq,:)=[Q'];
        S(n_eq,1)=N'*P0-Pz*nz*cos(tetha)-Py*ny*cos(tetha);
        CHECK(n_eq,:)=[W(n_eq,:)*SS S(n_eq,1)]; 

           end
        end
    end
    sol=W\S;
    [W\S SS ];


    u_sol=[sol(5) sol(7) sol(2)]';
    usol=[u_sol u];

    t_sol=[-sol(18)/sol(7) 0 sol(12)/sol(7)]';

    u_sol=u_sol/norm(u_sol);
    [u' t';u_sol' t_sol'];
    
    G=[1-u_sol(1)^2 -u_sol(1)*u_sol(2) -u_sol(1)*u_sol(3)
     %0 -u(3) u(2) 
     u_sol(1)^2-1 u_sol(1)*u_sol(2) u_sol(1)*u_sol(3)
     -u_sol(1)*u_sol(2) 1-u_sol(2)^2 -u_sol(2)*u_sol(3)
     %u(3) 0 -u(1)
     u_sol(1)*u_sol(2) u_sol(2)^2-1 u_sol(2)*u_sol(3)
     -u_sol(3)*u_sol(1) -u_sol(2)*u_sol(3) 1-u_sol(3)^2
     %-u(2) u(1) 0
     u_sol(3)*u_sol(1) u_sol(2)*u_sol(3) u_sol(3)^2-1];
    T=[sol(11) 
      %  sol(12) 
        sol(13) 
        sol(14) 
      %  sol(15) 
        sol(16) 
        sol(17) 
       % sol(18) 
        sol(19)];
    
    t_sol2=G\T;

   
    pos=max_phi;
    n_equations=0;
    for i=1:1
       for j=1:4

           if(j==1)
               pos=5;
           elseif(j==2)
               pos=10;
           elseif(j==3)
               pos=15;
           else
              pos=max_phi; 
           end
       n_equations=n_equations+1;
       n=nv(i,:)';
       R=axisRotation(u,rot_angles(pos));
       Pi=Pi_data(i,:,pos,max_theta)';
       P0=P0data(i,:)';

       M=n'*(R-eye(3));
       g=n'*P0-n'*R*Pi;
       
%        M_data(n_equations,:)=M;
%        g_data(n_equations,1)=g;
       %[M M*t g]
       
       r_2d=axisRotation([0 1 0]',rot_angles(pos));
       [n'*R*(Pi+t) pos];
       M_data(n_equations,:)=[n'*R 1];
       g_data(n_equations,1)=-n'*R*Pi;
       g_data2(n_equations,1)=g;
       end
    end
    %[M_data g_data t ]
    

    deg_err=acos(dot(u,u_sol))/pi*180;
    pos_err=norm(t-t_sol);
    pos_err_opt=pos_err;
    residual=inf;
    if(optimize==1)
        options = optimset('Algorithm','levenberg-marquardt');
        options = optimset(options,'MaxFunEvals', 99999); 
        
        Vars=[t_sol];
        [V residual]=lsqnonlin(@estimateErr,Vars,[],[],options);
        pos_err_opt=norm(t-V);
        [u' t';u_sol' t_sol';u_sol' V']
                
        u_draw=u_sol;
        t_draw=V;
    else
        u_draw=u_sol;
        t_draw=t_sol; 
    end
    
    close all;
    hold on;
   

    err=0;
    for orientation=1:1
       P0=P0data(orientation,:)';
       n_ox=-OX(orientation,:)';
       n_oy=-OY(orientation,:)';
       P1=P0+n_ox*pattern_size+n_oy*pattern_size;
       
       quiver3(P0(1),P0(2),P0(3),n_ox(1),n_ox(2),n_ox(3),pattern_size);
       quiver3(P0(1),P0(2),P0(3),n_oy(1),n_oy(2),n_oy(3),pattern_size);
       quiver3(P1(1),P1(2),P1(3),-n_ox(1),-n_ox(2),-n_ox(3),pattern_size);
       quiver3(P1(1),P1(2),P1(3),-n_oy(1),-n_oy(2),-n_oy(3),pattern_size);
       for i=1:maxphi(orientation)
           R=axisRotation(u_draw,rot_angles(i));
           %Si=R*t-t;
           %scatter3(Si(1),Si(2),Si(3),3,'r','filled');
           for j=1:maxtheta(orientation)
               
                 p=GT(orientation,:,i,j)';
                scatter3(p(1),p(2),p(3),4,'r','filled');
                 p2=R*(Pi_data(orientation,:,i,j)'+t_draw)-t_draw;
 
                 err=err+norm(p-p2);
                scatter3(p2(1),p2(2),p2(3),3,'g','filled');
           end
       end
    end
    grid on
    axis equal
    
    for i=1:length(angles)
        scatter3(Si(i,1),Si(i,2),Si(i,3),3,'b','filled');
        
    end
    
    errors=[err err/max_phi/max_theta residual pos_err_opt deg_err]
end

