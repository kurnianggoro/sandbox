clear;
close all;

sigma=0.12;
n_planes=30;

orientations=[2:n_planes];
distance=200;
radius=10;
pattern_size=50;
optimize=1;

% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Times New Roman')
set(0,'DefaultAxesFontSize', 14)

% Change default text fonts.
set(0,'DefaultTextFontname', 'Times New Roman')
set(0,'DefaultTextFontSize', 14)

trials=20;
k=0;
for par=orientations
    par
    k=k+1;
    for i=1:trials
       [orient,pos,pos_err_opt]=sim_calib(sigma, par, distance,radius,pattern_size,optimize);
       ori_err(i)=orient;
       pos_err(i)=pos_err_opt;
    end
    ERR_ori(k,:)=ori_err;
    ERR_pos(k,:)=pos_err;
    ori_mean_err(k)=mean(ori_err);
    pos_mean_err(k)=mean(pos_err);
end

save('orientations.mat');
varlist = {'pos_mean_err','ori_mean_err','ERR_pos','ERR_ori','pos_err','ori_err'};
clear(varlist{:})
disp('orientation completed')

sigmas=[0.12:0.01:0.3]
k=0;
for par=sigmas
    par
    k=k+1;
    for i=1:trials
       [orient,pos,pos_err_opt]=sim_calib(par, n_planes, distance,radius,pattern_size,optimize);
       ori_err(i)=orient;
       pos_err(i)=pos_err_opt;
    end
    ERR_ori(k,:)=ori_err;
    ERR_pos(k,:)=pos_err;
    ori_mean_err(k)=mean(ori_err);
    pos_mean_err(k)=mean(pos_err);
end

save('sigmas.mat');
varlist = {'pos_mean_err','ori_mean_err','ERR_pos','ERR_ori','pos_err','ori_err'};
clear(varlist{:})
disp('sigma completed')

distances=[100:10:300]
k=0;
for par=distances
    par
    k=k+1;
    for i=1:trials
       [orient,pos,pos_err_opt]=sim_calib(sigma, n_planes, par,radius,pattern_size,optimize);
       ori_err(i)=orient;
       pos_err(i)=pos_err_opt;
    end
    ERR_ori(k,:)=ori_err;
    ERR_pos(k,:)=pos_err;
    ori_mean_err(k)=mean(ori_err);
    pos_mean_err(k)=mean(pos_err);
end

save('distances.mat');
varlist = {'pos_mean_err','ori_mean_err','ERR_pos','ERR_ori','pos_err','ori_err'};
clear(varlist{:})
disp('distance completed')

pattern_sizes=[20:10:60]
k=0;
for par=pattern_sizes
    par
    k=k+1;
    for i=1:trials
       [orient,pos,pos_err_opt]=sim_calib(sigma, n_planes, distance,radius,par,optimize);
       ori_err(i)=orient;
       pos_err(i)=pos_err_opt;
    end
    ERR_ori(k,:)=ori_err;
    ERR_pos(k,:)=pos_err;
    ori_mean_err(k)=mean(ori_err);
    pos_mean_err(k)=mean(pos_err);
end

save('pattern_sizes.mat');
varlist = {'pos_mean_err','ori_mean_err','ERR_pos','ERR_ori','pos_err','ori_err'};
clear(varlist{:})
disp('size completed')