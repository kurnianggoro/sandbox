function err=computeErr(t)
    global P0data;
    global PiValid;
    global nv;  
    global u_sol;
    global orientations;
    global npts;
    
    u=u_sol;
    
    err=0;allpts=0;
    for o=1:orientations;
        for k=1:npts(o)
            allpts=allpts+1;
            Pi=PiValid.pts(:,o,k);
            P0=P0data(o,:)';
            u=u/norm(u);
            R=axisRotation(u,PiValid.phi(o,k));
            n=nv(o,:)';

            err1=abs(n'*(R*(Pi+t)-t-P0));
            penalty=dot(u,t)*100;
           % err2=((R*(Pi+t)-t)-((n'*t-t'*n*R*sv(j,:)')/((R*(Pi+t)-t)'*R'*n)))'...
           %     *((R*(Pi+t)-t)-((n'*t-t'*n*R*sv(j,:)')/((R*(Pi+t)-t)'*R'*n)));
            err=err+err1^2+penalty^2;
        
        end
    end
    err=err/allpts;