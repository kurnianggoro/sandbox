function err=estimateErr(t)
    global P0data;
    global Pi_data;
    global nv;  
    global u_sol;
    global rot_angles;
    global orientations;
    global max_phi;
    global max_theta;
    %global t;
    global sv;
    
%     u=x(1:3);
%     t=x(4:6);
    
    u=u_sol;
    
    err=0;npts=0;
    for o=1:orientations;
        for i=1:max_phi
            for j=2:max_theta
                npts=npts+1;
        Pi=Pi_data(o,:,10,10)';
        P0=P0data(o,:)';
        u=u/norm(u);
        R=axisRotation(u,rot_angles(10));
        n=nv(o,:)';

        err1=n'*(R*(Pi+t)-t-P0);
        penalty=dot(u,t)*100;
       % err2=((R*(Pi+t)-t)-((n'*t-t'*n*R*sv(j,:)')/((R*(Pi+t)-t)'*R'*n)))'...
       %     *((R*(Pi+t)-t)-((n'*t-t'*n*R*sv(j,:)')/((R*(Pi+t)-t)'*R'*n)));
        err=err+err1^2+penalty^2;
        
            end
        end
    end
    err=err/npts;
    %err